package top.fomeiherz.template;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 对{@link JdbcQuery}的改进，提供一个基本执行模板，让子类实现模板中定义的抽象方法。
 *
 * 参考：https://blog.csdn.net/qq_26222859/article/details/80292663
 */
public abstract class JdbcTemplate {

    // template method
    public final Object execute(String sql) throws SQLException {
        Connection con = DriverManager.getConnection("");
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            Object result = doInStatement(rs);//abstract method
            return result;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (!con.isClosed()) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    // implements in subclass
    protected abstract Object doInStatement(ResultSet rs);
}

class JdbcTemplateUserImpl extends JdbcTemplate {

    @Override
    protected Object doInStatement(ResultSet rs) {
        List<User> userList = new ArrayList<User>();

        try {
            User user = null;
            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt("id"));
                user.setUserName(rs.getString("user_name"));
                user.setBirth(rs.getDate("birth"));
                user.setCreateDate(rs.getDate("create_date"));
                userList.add(user);
            }
            return userList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}

class User {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public java.util.Date getBirth() {
        return birth;
    }

    public void setBirth(java.util.Date birth) {
        this.birth = birth;
    }

    public java.util.Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
    }

    private int id;
    private String userName;
    private java.util.Date birth;
    private Date createDate;
}
