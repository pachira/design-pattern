package top.fomeiherz.template;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 对{@link JdbcTemplate}的改进，不用继承了，通过传入接口实现即可。
 */
public class SpringJdbcTemplate {
    //template method
    private final Object execute(StatementCallback action) throws SQLException {
        Connection con = DriverManager.getConnection("");
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            // abstract method
            Object result = action.doInStatement(stmt);
            return result;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        } finally {

            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (!con.isClosed()) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    public Object query(StatementCallback stmt) throws SQLException {
        return execute(stmt);
    }
}

@FunctionalInterface
interface StatementCallback {
    Object doInStatement(Statement stmt) throws SQLException;
}


