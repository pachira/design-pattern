package top.fomeiherz.lod;

public interface Deserializable {
    Object deserialize(String text);
}
