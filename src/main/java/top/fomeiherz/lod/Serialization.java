package top.fomeiherz.lod;

/**
 * 原则：“接口使用分离，实现使用内聚”。“接口使用分离”使得依赖类只依赖它必要接口，“实现使用内聚”使得修改不至于很分散。
 */
public class Serialization implements Serializable, Deserializable  {
    public Object deserialize(String text) {
        // do something
        return null;
    }

    public String serialize(Object object) {
        // do something
        return null;
    }
}
