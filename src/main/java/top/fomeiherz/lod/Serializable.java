package top.fomeiherz.lod;

public interface Serializable {
    String serialize(Object object);
}
