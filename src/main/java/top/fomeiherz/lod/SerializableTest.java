package top.fomeiherz.lod;

public class SerializableTest {

    // 依赖接口而非实现
    private Serializable serializable;

    public SerializableTest(Serializable serializable) {
        this.serializable = serializable;
    }

    public String serialize(Object object) {
        return serializable.serialize(object);
    }

}
