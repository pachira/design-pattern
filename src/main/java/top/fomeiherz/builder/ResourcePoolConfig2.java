package top.fomeiherz.builder;

import lombok.Builder;

/**
 * TODO
 *
 * @author fomeiherz
 * @date 2020/3/22 15:10
 */
@Builder
public class ResourcePoolConfig2 {
    private int maxIdle;
    private int minIdle;
}

/**
 * 生成代码如下
 */
//public class ResourcePoolConfig2 {
//    private int maxIdle;
//    private int minIdle;
//
//    ResourcePoolConfig2(int maxIdle, int minIdle) {
//        this.maxIdle = maxIdle;
//        this.minIdle = minIdle;
//    }
//
//    public static ResourcePoolConfig2.ResourcePoolConfig2Builder builder() {
//        return new ResourcePoolConfig2.ResourcePoolConfig2Builder();
//    }
//
//    public static class ResourcePoolConfig2Builder {
//        private int maxIdle;
//        private int minIdle;
//
//        ResourcePoolConfig2Builder() {
//        }
//
//        public ResourcePoolConfig2.ResourcePoolConfig2Builder maxIdle(int maxIdle) {
//            this.maxIdle = maxIdle;
//            return this;
//        }
//
//        public ResourcePoolConfig2.ResourcePoolConfig2Builder minIdle(int minIdle) {
//            this.minIdle = minIdle;
//            return this;
//        }
//
//        public ResourcePoolConfig2 build() {
//            return new ResourcePoolConfig2(this.maxIdle, this.minIdle);
//        }
//
//        public String toString() {
//            return "ResourcePoolConfig2.ResourcePoolConfig2Builder(maxIdle=" + this.maxIdle + ", minIdle=" + this.minIdle + ")";
//        }
//    }
//}
