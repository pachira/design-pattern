package top.fomeiherz.builder;

/**
 * 建造者模式创建对象
 *
 * @author fomeiherz
 * @date 2020/3/22 14:57
 */
public class ResourcePoolConfig {

    private int maxIdle;
    private int minIdle;
    
    /**
     * 私有构造方法，不允许外部直接new对象，只能通过builder来创建
     */
    private ResourcePoolConfig(Builder builder) {
        this.maxIdle = builder.maxIdle;
        this.minIdle = builder.minIdle;
    }

    public int getMaxIdle() {
        return maxIdle;
    }

    public int getMinIdle() {
        return minIdle;
    }

    public static class Builder {
        // 参数需要在这里再定义一遍，其实增加了工作量
        private int maxIdle;
        private int minIdle;
        
        public ResourcePoolConfig build() {
            // 属性之间有依赖关系，统一都在build()方法中判断
            if (this.maxIdle < this.minIdle) {
                throw new RuntimeException("MaxIdle must greater then minIdle.");
            }
            return new ResourcePoolConfig(this);
        }

        public int getMaxIdle() {
            return maxIdle;
        }

        public Builder setMaxIdle(int maxIdle) {
            this.maxIdle = maxIdle;
            return this;
        }

        public int getMinIdle() {
            return minIdle;
        }

        public Builder setMinIdle(int minIdle) {
            this.minIdle = minIdle;
            return this;
        }
    }
    
    
}
