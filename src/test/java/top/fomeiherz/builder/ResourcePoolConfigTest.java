package top.fomeiherz.builder;

/**
 * TODO
 *
 * @author fomeiherz
 * @date 2020/3/22 15:06
 */
public class ResourcePoolConfigTest {

    public static void main(String[] args) {
        ResourcePoolConfig config = new ResourcePoolConfig.Builder()
                .setMaxIdle(10)
                .setMinIdle(15)
                .build();
        System.out.println(config);
    }

}
