package top.fomeiherz;

import java.util.List;
import java.util.Map;

/**
 * Redis存储原始数据
 *
 * @author fomeiherz
 * @date 2020/2/15 22:42
 */
public class RedisMetricsStorage implements MetricsStorage {

    @Override
    public void save(RequestInfo info) {

    }

    @Override
    public List<RequestInfo> getByApiNameAndTimestamp(String apiName, long startTimeInMillis, long endTimeInMillis) {
        return null;
    }

    public Map<String, List<RequestInfo>> getByTimestamp(long startTimeInMillis, long endTimeInMillis) {
        return null;
    }

}
