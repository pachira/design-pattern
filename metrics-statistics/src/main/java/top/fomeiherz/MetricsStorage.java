package top.fomeiherz;

import java.util.List;
import java.util.Map;

/**
 * 原始数据存储
 *
 * @author fomeiherz
 * @date 2020/2/15 22:42
 */
public interface MetricsStorage {

    void save(RequestInfo info);

    List<RequestInfo> getByApiNameAndTimestamp(String apiName, long startTimeInMillis, long endTimeInMillis);

    Map<String, List<RequestInfo>> getByTimestamp(long startTimeInMillis, long endTimeInMillis);

}
