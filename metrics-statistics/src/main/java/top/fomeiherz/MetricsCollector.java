package top.fomeiherz;

import com.mysql.cj.util.StringUtils;

/**
 * 采集接口的请求的原始数据
 *
 * @author fomeiherz
 * @date 2020/2/15 22:37
 */
public class MetricsCollector {

    // 基于接口而非实现编程，灵活修改存储方式
    private MetricsStorage metricsStorage;
    // 依赖注入
    public MetricsCollector(MetricsStorage metricsStorage) {
        this.metricsStorage = metricsStorage;
    }

    // 保存请求记录
    public void recordRequest(RequestInfo requestInfo) {
        if (requestInfo == null || StringUtils.isNullOrEmpty(requestInfo.getApiName())) {
            return;
        }
        metricsStorage.save(requestInfo);
    }
}
